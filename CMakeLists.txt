cmake_minimum_required(VERSION 3.14)
project(newton-square)

set(CMAKE_CXX_STANDARD 17)

add_executable(network_sqrt main.cpp newton.h)