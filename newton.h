#include "math.h"



class Newton
{

public:


    double newtonSquare(double number, double value)
    {
        return pow(number,2) - value;
    }

    double derivative(double x)
    {
        return x*2;
    }


    double calculate(double square,double initGuess,double guessLimitCount)
    {

        double temp = initGuess;
        for (int i = 0; i < guessLimitCount; ++i) {

            temp = temp - (newtonSquare(temp,square) / derivative(temp));
        }

        return temp;

    }





};